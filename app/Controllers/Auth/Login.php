<?php

namespace App\Controllers\Auth;

use App\Controllers\BaseController;
use App\Models\UsersModel;

class Login extends BaseController
{
    protected $users;

    function __construct()
    {
        $this->users = new UsersModel();
    }
    public function index()
    {
        return view('auth/login');
    }

    public function doLogin()
    {
        $username = $this->request->getVar('username');
        $password = $this->request->getVar('password');
        $dataUser = $this->users->where([
            'username' => $username,
            'is_aktif' => '1',
        ])->first();
        if ($dataUser) {
            if (password_verify($password, $dataUser->password)) {
                session()->set([
                    'id_user' => $dataUser->id_user,
                    'id_level' => $dataUser->id_level,
                    'email' => $dataUser->email,
                    'logged_in' => TRUE
                ]);
                log_message('error', $dataUser->id_user);
                return redirect()->to('/main');
            } else {
                session()->setFlashdata('error', 'Username & Password Tidak Sesuai');
                return redirect()->back();
            }
        } else {
            session()->setFlashdata('error', 'Username & Password Tidak Sesuai');
            return redirect()->back();
        }
    }

    public function doLogout()
    {
        session()->destroy();
        return redirect()->to('/login');
    }
}
