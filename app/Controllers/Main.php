<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Main extends BaseController
{
    public function index()
    {
        if (session()->get('id_level') == 1) {
            return view('perawat/index');
        } else {
            return view('admin/index');
        }
    }
}
