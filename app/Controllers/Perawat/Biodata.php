<?php

namespace App\Controllers\Perawat;

use App\Controllers\BaseController;

class Biodata extends BaseController
{
    public function index()
    {
        return view('perawat/edit');
    }

    public function edit()
    {
        return view('perawat/biodata/edit');
    }
}
