<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="card">
            <div class="card-body login-card-body">
                <img src="<?= base_url('assets/img/ci.webp') ?>" class="img-fluid mx-auto d-block" width="200" height="200">
                <p class="login-box-msg">Sign in to start your session</p>
                <?php
                if (!empty(session()->getFlashdata('error'))) {
                ?>
                    <div class="alert alert-danger">
                        <?= session()->getFlashdata('error'); ?>
                    </div>
                <?php
                }
                ?>
                <form action="<?= base_url('/doLogin'); ?>" method="post">
                    <?= csrf_field(); ?>
                    <div class="input-group mb-3">
                        <input id="username" name="username" type="text" class="form-control" placeholder="username" autocomplete="nope" autofocus required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fa fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-2">
                        <input id="password" name="password" type="password" class="form-control" placeholder="Password" autocomplete="new-password" required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="mb-4">
                        <label class="checkbox-inline" style="cursor:pointer; user-select: none; font-weight: normal;"><input type="checkbox" style="cursor:pointer;" onclick="showPassword('password')"> Show Password</label>
                    </div>
                    <input id="submitSignIn" type="submit" class="btn btn-primary btn-block" value="Sign In">
                </form>
            </div>
        </div>
    </div>
</body>
<?= $this->endSection('content'); ?>

<script>
    function showPassword(id) {
        var x = document.getElementById(id);
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
</script>
