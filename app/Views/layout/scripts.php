<script src="<?= base_url('assets/adminLTE/plugins/jquery/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/adminLTE/plugins/jquery-ui/jquery-ui.min.js') ?>"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<script src="<?= base_url('assets/adminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
<script src="<?= base_url('assets/adminLTE/plugins/chart.js/Chart.min.js') ?>"></script>
<script src="<?= base_url('assets/adminLTE/plugins/sparklines/sparkline.js') ?>"></script>
<script src="<?= base_url('assets/adminLTE/plugins/jqvmap/jquery.vmap.min.js') ?>"></script>
<script src="<?= base_url('assets/adminLTE/plugins/jqvmap/maps/jquery.vmap.usa.js') ?>"></script>
<script src="<?= base_url('assets/adminLTE/plugins/jquery-knob/jquery.knob.min.js') ?>"></script>
<script src="<?= base_url('assets/adminLTE/plugins/moment/moment.min.js') ?>"></script>
<script src="<?= base_url('assets/adminLTE/plugins/daterangepicker/daterangepicker.js') ?>"></script>
<script src="<?= base_url('assets/adminLTE/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') ?>"></script>
<script src="<?= base_url('assets/adminLTE/plugins/summernote/summernote-bs4.min.js') ?>"></script>
<script src="<?= base_url('assets/adminLTE/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') ?>"></script>
<script src="<?= base_url('assets/adminLTE/dist/js/adminlte.js') ?>"></script>
<script src="<?= base_url('assets/adminLTE/dist/js/pages/dashboard.js') ?>"></script>
<script src="<?= base_url('assets/adminLTE/dist/js/demo.js') ?>"></script>
