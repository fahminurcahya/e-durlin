<!DOCTYPE html>
<html>
<?= $this->include('layout/head'); ?>
<?php if (session()->get('logged_in')) : ?>

    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <?= $this->include('layout/nav'); ?>
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                    </div>
                </div>
                <section class="content">
                    <div class="container-fluid">
                        <?= $this->renderSection('content'); ?>
                    </div>
                </section>
            </div>
            <?= $this->include('layout/footer'); ?>
        </div>
        <?= $this->include('layout/scripts'); ?>
        <?= $this->renderSection('scripts'); ?>
    </body>
<?php else : ?>
    <?= $this->renderSection('content'); ?>
<?php endif; ?>



</html>
