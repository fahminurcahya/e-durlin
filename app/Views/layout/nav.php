<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?= base_url('assets/img/ci.webp') ?>" class="img-circle" height="40px" width="40px" alt="User Image">
            </a>
            <ul class="dropdown-menu">
                <li class="user-header">
                    <img src="<?= base_url('assets/img/ci.webp') ?>" class="img-circle " alt="User Image">
                    <p>
                        <!-- {{.User.Username}} -->
                        <small id="date"></small>
                    </p>
                </li>
                <li class="user-footer">
                    <div class="d-flex justify-content-between">
                        <div class="pull-left">
                            <a class="btn btn-default">Change Password</a>
                        </div>
                        <div class="pull-right">
                            <a href="<?= base_url('doLogout') ?>" class="btn btn-default">Sign out</a>
                        </div>
                    </div>
                </li>
            </ul>
        </li>
    </ul>
</nav>
<!-- /.navbar -->

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="<?= base_url('assets/img/ci.webp') ?>" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">APP NAME</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Nama Pegawai</a>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <?php if (session()->get('id_level') == 1) : ?>
                    <li class="nav-item">
                        <a href="<?= base_url('main') ?>" class="nav-link">
                            <i class="nav-icon far fa-address-card"></i>
                            <p>
                                Akun Pribadi
                                <span class="right badge badge-danger fa fa-bell"> notif</span>
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="pages/gallery.html" class="nav-link">
                            <i class="nav-icon fas fa-book"></i>
                            <p>
                                Petunjuk Penggunaan
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="pages/gallery.html" class="nav-link">
                            <i class="nav-icon fas fa-envelope"></i>
                            <p>
                                Feedback Sistem
                            </p>
                        </a>
                    </li>
                <?php elseif (session()->get('id_level') == 2) : ?>
                    <li class="nav-item">
                        <a href="pages/gallery.html" class="nav-link">
                            <i class="nav-icon far fa-address-card"></i>
                            <p>
                                Akun Pribadi 2
                                <span class="right badge badge-danger fa fa-bell"> notif</span>
                            </p>
                        </a>
                    </li>
                <?php elseif (session()->get('id_level') == 3) : ?>
                    <li class="nav-item">
                        <a href="pages/gallery.html" class="nav-link">
                            <i class="nav-icon far fa-address-card"></i>
                            <p>
                                Akun Pribadi 3
                                <span class="right badge badge-danger fa fa-bell"> notif</span>
                            </p>
                        </a>
                    </li>
                <?php elseif (session()->get('id_level') == 4) : ?>
                    <li class="nav-item">
                        <a href="pages/gallery.html" class="nav-link">
                            <i class="nav-icon far fa-address-card"></i>
                            <p>
                                Akun Pribadi 4
                                <span class="right badge badge-danger fa fa-bell"> notif</span>
                            </p>
                        </a>
                    </li>
                <?php elseif (session()->get('id_level') == 5) : ?>
                    <li class="nav-item">
                        <a href="pages/gallery.html" class="nav-link">
                            <i class="nav-icon far fa-address-card"></i>
                            <p>
                                Akun Pribadi 5
                                <span class="right badge badge-danger fa fa-bell"> notif</span>
                            </p>
                        </a>
                    </li>
                <?php elseif (session()->get('id_level') == 6) : ?>
                    <li class="nav-item">
                        <a href="pages/gallery.html" class="nav-link">
                            <i class="nav-icon far fa-address-card"></i>
                            <p>
                                Akun Pribadi 6
                                <span class="right badge badge-danger fa fa-bell"> notif</span>
                            </p>
                        </a>
                    </li>
                <?php endif; ?>
            </ul>
        </nav>






        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
