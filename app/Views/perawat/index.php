<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>
<div class="callout callout-danger">
    <h5><i class="fas fa-info"></i> Informasi:</h5>
    Segera urus DUPAK/Naik Level/UKOM PNS/STR expired jatuh tempo
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card card-widget widget-user-2">
            <div class="widget-user-header">
                <div class="row">
                    <div class="col-md-1 widget-user-image">
                        <img class="img-circle elevation-2" src="<?= base_url('assets/img/ci.webp') ?>" alt="User Avatar">
                    </div>
                    <div class="col-md-10">
                        <h3 class="">Nama Pegawai</h3>
                        <h5 class="">Nurse - 15046</h5>
                    </div>
                </div>
            </div>
            <div class="card-footer p-0">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            Biodata Diri
                            <button class="float-right btn-sm btn-info" style="margin-bottom: 8px;">Cetak</button>
                            <button class="float-right btn-sm btn-warning" style="margin-right: 5px; margin-bottom: 8px;" onclick="ubah()">Ubah</button>
                            <button class="float-right btn-sm btn-primary" style="margin-right: 5px; margin-bottom: 8px;">Lihat</button>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            Logbook
                            <button class="float-right btn-sm btn-info" style="margin-bottom: 8px;">Cetak</button>
                            <button class="float-right btn-sm btn-warning" style="margin-right: 5px; margin-bottom: 8px;">Ubah</button>
                            <button class="float-right btn-sm btn-primary" style="margin-right: 5px; margin-bottom: 8px;">Lihat</button>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            DUPAK
                            <button class="float-right btn-sm btn-info" style="margin-bottom: 8px;">Cetak</button>
                            <button class="float-right btn-sm btn-warning" style="margin-right: 5px; margin-bottom: 8px;">Ubah</button>
                            <button class="float-right btn-sm btn-primary" style="margin-right: 5px; margin-bottom: 8px;">Lihat</button>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            Pengajuan <span class=" badge bg-danger fa fa-bell"> notif</span>
                            <button class="float-right btn-sm btn-info" style="margin-bottom: 8px;">Cetak</button>
                            <button class="float-right btn-sm btn-warning" style="margin-right: 5px; margin-bottom: 8px;">Ubah</button>
                            <button class="float-right btn-sm btn-primary" style="margin-right: 5px; margin-bottom: 8px;">Lihat</button>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            Konsultasi <span class=" badge bg-danger fa fa-bell"> notif</span>
                            <button class="float-right btn-sm btn-info" style="margin-bottom: 8px;">Cetak</button>
                            <button class="float-right btn-sm btn-warning" style="margin-right: 5px; margin-bottom: 8px;">Ubah</button>
                            <button class="float-right btn-sm btn-primary" style="margin-right: 5px; margin-bottom: 8px;">Lihat</button>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            Kotak Surat <span class="badge bg-danger fa fa-bell"> notif</span>
                            <button class="float-right btn-sm btn-info" style="margin-bottom: 8px;">Cetak</button>
                            <button class="float-right btn-sm btn-warning" style="margin-right: 5px; margin-bottom: 8px;">Ubah</button>
                            <button class="float-right btn-sm btn-primary" style="margin-right: 5px; margin-bottom: 8px;">Lihat</button>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- ./wrapper -->
<?= $this->endSection('content'); ?>

<?= $this->section('scripts'); ?>
<script>
    function ubah() {
        location.href = "<?= base_url('biodata/edit') ?>";
    }
</script>
<?= $this->endSection('scripts'); ?>
