<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;

class UserSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id_user' => 1,
                'username' => 'perawat',
                'password' => password_hash('perawat', PASSWORD_BCRYPT),
                'email' => 'perawat@gmail.com',
                'id_level' => 1,
                'created_at' => Time::now()
            ],
            [
                'id_user' => 2,
                'username' => 'jakung',
                'password' => password_hash('jakung', PASSWORD_BCRYPT),
                'email' => 'jakung@gmail.com',
                'id_level' => 2,
                'created_at' => Time::now()
            ],
            [
                'id_user' => 3,
                'username' => 'komite',
                'password' => password_hash('komite', PASSWORD_BCRYPT),
                'email' => 'komite@gmail.com',
                'id_level' => 3,
                'created_at' => Time::now()
            ],
            [
                'id_user' => 4,
                'username' => 'sdm',
                'password' => password_hash('sdm', PASSWORD_BCRYPT),
                'email' => 'sdm@gmail.com',
                'id_level' => 4,
                'created_at' => Time::now()
            ],
            [
                'id_user' => 5,
                'username' => 'pelayanan',
                'password' => password_hash('pelayanan', PASSWORD_BCRYPT),
                'email' => 'pelayanan@gmail.com',
                'id_level' => 5,
                'created_at' => Time::now()
            ],
            [
                'id_user' => 6,
                'username' => 'kepala',
                'password' => password_hash('kepala', PASSWORD_BCRYPT),
                'email' => 'kepala@gmail.com',
                'id_level' => 6,
                'created_at' => Time::now()
            ]
        ];
        $this->db->table('users')->insertBatch($data);
    }
}
