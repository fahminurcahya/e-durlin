<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;


class LevelSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id_level' => 1,
                'level' => 'Perawat',
                'created_at' => Time::now()
            ],
            [
                'id_level' => 2,
                'level' => 'Jabatan Fungsional',
                'created_at' => Time::now()
            ],
            [
                'id_level' => 3,
                'level' => 'Komite Keperawatan',
                'created_at' => Time::now()
            ],
            [
                'id_level' => 4,
                'level' => 'SDM',
                'created_at' => Time::now()
            ],
            [
                'id_level' => 5,
                'level' => 'Bidang Pelayanan Keperawatan',
                'created_at' => Time::now()
            ],
            [
                'id_level' => 6,
                'level' => 'Kepala Unit',
                'created_at' => Time::now()
            ]
        ];
        $this->db->table('level')->insertBatch($data);
    }
}
